module git.sr.ht/~rumpelsepp/promalerter

go 1.15

require (
	github.com/pelletier/go-toml v1.8.1
	github.com/prometheus/alertmanager v0.21.0
)
