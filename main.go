package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"text/template"
	"time"

	"github.com/pelletier/go-toml"
	alert "github.com/prometheus/alertmanager/template"
)

type config struct {
	Command       string
	ListenAddress string
	Template      string
	Shell         string
}

func defaultConfig() *config {
	return &config{
		Command:       "",
		Shell:         "/bin/sh",
		ListenAddress: "127.0.0.1:5001",
		Template:      `{{range .Alerts -}}
* {{ .Status }}: {{index .Annotations "summary"}}{{ end -}}`,
	}
}

var globalConfigPath = "/etc/promalerter/config.toml"

func configPath() string {
	p, err := os.UserConfigDir()
	if err != nil {
		panic(err)
	}
	return filepath.Join(p, "promalerter", "config.toml")
}

func loadConfig() (*config, error) {
	var (
		err  error
		file *os.File
	)
	file, err = os.Open(configPath())
	if err != nil {
		file, err = os.Open(globalConfigPath)
		if err != nil {
			return nil, err
		}
	}
	confStr, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}
	var conf config
	if err := toml.Unmarshal(confStr, &conf); err != nil {
		return nil, err
	}
	return &conf, nil
}

type webHookServer struct {
	command  string
	shell    string
	template *template.Template
}

func (s *webHookServer) handleAlert(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "method not allowed", http.StatusMethodNotAllowed)
		return
	}

	var a alert.Data
	if err := json.NewDecoder(r.Body).Decode(&a); err != nil {
		fmt.Println(err)
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}

	var (
		cmd *exec.Cmd
		buf bytes.Buffer
	)
	if err := s.template.Execute(&buf, a); err != nil {
		fmt.Println(err)
		http.Error(w, "template error", http.StatusInternalServerError)
		return
	}
	if s.command != "" {
		if s.shell != "" {
			cmd = exec.Command(s.shell, "-c", s.command)
		} else {
			cmdList := strings.Split(s.command, " ")
			cmd = exec.Command(cmdList[0], cmdList[1:]...)
		}
		cmd.Stdin = &buf
		if err := cmd.Run(); err != nil {
			fmt.Println(err)
			http.Error(w, "command failed", http.StatusInternalServerError)
			return
		}
	} else {
		fmt.Println(string(buf.Bytes()))
	}
}

func main() {
	var (
		handler     = http.NewServeMux()
		defaultConf = defaultConfig()
	)
	conf, err := loadConfig()
	if err != nil {
		fmt.Println("no config, loading defaults")
		conf = defaultConf
	}
	if conf.ListenAddress == "" {
		conf.ListenAddress = defaultConf.ListenAddress
	}
	if conf.Shell == "" {
		conf.Shell = defaultConf.Shell
	}
	if conf.Template == "" {
		conf.Template = defaultConf.Template
	}

	t, err := template.New("alert").Parse(conf.Template)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	whSrv := webHookServer{
		command:  conf.Command,
		shell:    conf.Shell,
		template: t,
	}
	handler.HandleFunc("/", whSrv.handleAlert)
	srv := &http.Server{
		Addr:         conf.ListenAddress,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      handler,
	}
	if err := srv.ListenAndServe(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
