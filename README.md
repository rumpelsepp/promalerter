# promalerter

This is a simple program which listens for [prometheus webhooks](https://www.prometheus.io/docs/alerting/latest/configuration/#webhook_config) and forwards the alert to a customizable command.
It spawns the configured command and copies the alert to the command's stdin stream.

## Examples of suitable programs

* [mnotify](https://sr.ht/~rumpelsepp/mnotify): Send alerts to Matrix rooms.
* sendmail: The classic server side email interface.

## Config

The config lives at `~/.config/promalerter/config.toml` and accepts the following settings:

* Command (string): The command to execute for each alert. If it's empty `promalerter` prints to stdout.
* ListenAddress (string): The address and port to listen on. Default: `127.0.0.1:5001`.
* Template (string): A go [text template](https://golang.org/pkg/text/template). The passed data structure is a [prometheus alert data](https://pkg.go.dev/github.com/prometheus/alertmanager@v0.21.0/template#Data).
* Shell (string): The shell to use for the command. Set to `""` to disable the spawning of a shell. Default is `/bin/sh`.
